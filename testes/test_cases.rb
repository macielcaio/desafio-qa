require_relative 'checkout_challenge'
require_relative 'values'
require 'test/unit'

class TestPrice < Test::Unit::TestCase
  def price(goods)
    co = CheckoutCode.new(VALUES)
    goods.split(//).each { |item| co.scan(item) }
    co.total
  end

  def total_values
    assert_equal(0, price(''))
    assert_equal(50, price('A'))
    assert_equal(80, price('AB'))
    assert_equal(115, price('CDBA'))

    assert_equal(100, price('AA'))
    assert_equal(130, price('AAA'))
    assert_equal(180, price('AAAA'))

    assert_equal(160, price('AAAB'))
    assert_equal(175, price('AAABB'))
    assert_equal(190, price('DBBAAA'))
    assert_equal(190, price('AABBD'))
  end

  def test_valids
    co = CheckoutCode.new(VALUES)
    assert_equal(0, co.total)

    co.scan('A')
    assert_equal(50, co.total)

    co.scan('B')
    assert_equal(80, co.total)

    co.scan('A')
    assert_equal(130, co.total)

    co.scan('A')
    assert_equal(160, co.total)

    co.scan('B')
    assert_equal(175, co.total)
  end
end
