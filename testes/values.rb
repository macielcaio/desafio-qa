VALUES = {
  'A' => DiscountRules.new(50, Discount.new(20, 3)),
  'B' => DiscountRules.new(30, Discount.new(15, 2)),
  'C' => DiscountRules.new(20),
  'D' => DiscountRules.new(15)
}.freeze
