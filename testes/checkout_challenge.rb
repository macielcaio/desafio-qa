class Discount
  def initialize(value, quantity)
    @value = value
    @quantity = quantity
  end

  def calculate_quant(quantity)
    (quantity / @quantity).floor * @value
  end
end

class DiscountRules
  def initialize(init, *discounts_values)
    @init = init
    @discounts_values = discounts_values
  end

  def calculate(quantity)
    quantity * @init - calc_discount(quantity)
  end

  def calc_discount(quantity)
    @discounts_values.inject(0) do |val, discount|
      val + discount.calculate_quant(quantity)
    end
  end
end

class CheckoutCode
  def initialize(rules)
    @rules = rules
    @items = {}
  end

  def scan(item)
    @items[item] ||= 0
    @items[item] += 1
  end

  def total
    @items.inject(0) do |val, (item, quantity)|
      val + calculate(item, quantity)
    end
  end

  private

  def calculate(item, quantity)
    if rules(item)
      rules(item).calculate(quantity)
    else
      raise "Could not found a valid item: '#{item}'"
    end
  end

  def rules(item)
    @rules[item]
  end
end
