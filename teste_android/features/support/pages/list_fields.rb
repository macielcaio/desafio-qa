class ListFields

  include Calabash::Android::Operations

  def users_call_list
    raise("Could not found contacts call list") if query("* id:'#contacts'").count > 1
  end

  def users_contact_list
    raise("Could not found contact list") if element_exists("* label:'New group'") == false
  end

  def users_contact_page
    wait_for_elements_exist(["* label:'Name'", "* label:'Company'", "* label:'Title'"])
  end
end
