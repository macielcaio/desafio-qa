class MainActions

  include Calabash::Android::Operations

  def wait_and_touch(element)
    wait_for_element_exists("* label:'#{element}'")
    touch("* label:'#{element}'")
  end

  def touch_first(element)
    wait_for_element_exists("* label:'#{element}'")
    touch("* label:'#{element}'", index: 0)
  end

  def wait_and_touch_element(element)
    wait_for_element_exists("* id:'#{element}'")
    touch("* id:'#{element}'")
  end

end
