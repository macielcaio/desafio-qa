Feature: WhatsApp Pictures Validations

  Scenario: Validate whatsApp picture loading from an valid user
    Given The app has been launched with success
    When I tap the "CONTACTS" tab
    And I tap the first "#user"
    Then I expect to see only one picture in current screen

  Scenario: Validate whatsApp pictures loading from contacts list
    Given The app has been launched with success
    When I tap the "CONTACTS" tab
    And I scroll down
    Then I expect to see more than one picture in current screen

  Scenario: Validate whatsApp current user picture
    Given The app has been launched with success
    When I tap in "#options"
    And I tap in "Settings"
    Then I expect to see the following picture "#myUser"

