Feature: WhatsApp Tabs Validations

  Scenario: Validate whatsApp CALLS tab functions
    Given The app has been launched with success
    When I tap the "CALLS" tab
    And I tap in "#call"
    Then I expect to see a list of users to call

  Scenario: Validate whatsApp CONTACTS tab functions
    Given The app has been launched with success
    When I tap the "CHATS" tab
    And I tap in "#conversations"
    Then I expect to see my contact list

  Scenario: Validate whatsApp CHATS tab functions
    Given The app has been launched with success
    When I tap the "CONTACTS" tab
    And I tap in "#addUser"
    Then I expect to see the save contact page

