Feature: WhatsApp Privacy Validations

  Background:
    Given The app has been launched with success
    And I tap in "#options"
    And I tap in "Settings"
    And I tap in "Privacy"

  Scenario: Validate whatsApp lastSeen options in privacy
    When I tap in "Last seen"
    And I tap in "Everyone"
    Then I expect to see "Everyone"

  Scenario: Validate whatsApp Profile photo options in privacy
    When I tap in "Profile photo"
    And I tap in "My contacts"
    Then I expect to see "My contacts"

  Scenario: Validate whatsApp Status options in privacy
    When I tap in "Status"
    And I tap in "Nobody"
    Then I expect to see "Nobody"
