Given(/^The app has been launched with success$/) do
  wait_for_element_exists("* label:'WhatsApp'")
end

When(/^I tap the "([^"]*)" tab$/) do |arg1|
  MainActions.new.wait_and_touch(arg1)
end

When(/^I tap the first "([^"]*)"$/) do |arg1|
  MainActions.new.touch_first(arg1)
end

Then(/^I expect to see only one picture in current screen$/) do
  raise("There is more than one picture in current screen") if query("UIImageView").count != 1
end

When(/^I scroll down$/) do
  scroll(:down)
end

Then(/^I expect to see more than one picture in current screen$/) do
  raise("There is only one picture in current screen") if query("UIImageView").count < 1
end

When(/^I tap in "([^"]*)"$/) do |arg1|
  MainActions.new.wait_and_touch_element(arg1)
end

Then(/^I expect to see the following picture "([^"]*)"$/) do |arg1|
  raise("Could not found the defined picture") if query("UIImageView *:'#{arg1}'") == nil
end

Then(/^I expect to see "([^"]*)"$/) do |arg1|
  wait_for_element_exists(arg1)
end

Then(/^I expect to see a list of users to call$/) do
  ListFields.new.users_call_list
end

Then(/^I expect to see my contact list$/) do
  ListFields.new.users_contact_list
end

Then(/^I expect to see the save contact page$/) do
  ListFields.new.users_contact_page
end
